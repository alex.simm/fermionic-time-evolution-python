from typing import List

import numpy as np

from src.Hamiltonian import Hamiltonian
from src.observables.Observable import Observable
from src.State import State


class TimeEvolution:
    # The simulated system
    _system: Hamiltonian
    # Current state vector
    _psi: np.ndarray
    # Current time
    __time: float
    # Time points during evolution
    __times: List[float]

    def __init__(self, system: Hamiltonian):
        self._system = system
        self._psi = np.zeros(system.getBasis().dim())
        self.__time = 0
        self.__times = []

    def getHamiltonian(self) -> Hamiltonian:
        return self._system

    def getPsi(self) -> np.ndarray:
        """ Returns the current state vector. """
        return self._psi

    def setPsi(self, psi: np.ndarray) -> None:
        """
        Sets the current state vector.
        @throws runtime_error if the dimension of the state does not match the Hilbert or Fock space
        """
        if psi.shape[0] != self._system.getBasis().dim():
            raise RuntimeError(
                f"wrong size of new state vector psi: {str(psi.shape[0])} != {str(self._system.getBasis().dim())}")
        self._psi = psi

    def setPsiState(self, state: State) -> None:
        """
        Sets the current state vector to the value corresponding to a basis state. The basis state must be in
        the Hilbert or Fock space of the system.
        """
        self._psi.fill(0.0)
        self._psi[self._system.getBasis().getStateIndex(state)] = 1

    def setPsiStates(self, states: List[State]) -> None:
        """
        Sets the current state vector to the values corresponding to a superposition of basis states. All
        basis states will have the same weight and must be in the Hilbert or Fock space of the system.
        """
        self.setPsiStates2(states, [1.0] * len(states))

    def setPsiStates2(self, states: List[State], weights: List[complex]) -> None:
        """
        Sets the current state vector to the values corresponding to a superposition of basis states with
        different weights. All basis states will have the same weight and must be in the Hilbert or Fock space
        of the system. The weights will be normalised.
        """
        if len(states) != len(weights):
            raise RuntimeError("List of states and list of weights have different sizes")

        basis = self._system.getBasis()
        self._psi.fill(0.0)
        for i in range(len(states)):
            self._psi[basis.getStateIndex(states[i])] = weights[i]
        self._psi = self._psi / np.linalg.norm(self._psi)

    def getTime(self) -> float:
        """ Returns the current time of the simulation """
        return self.__time

    def getTimes(self) -> List[float]:
        """ Returns all current time points of the simulation """
        return self.__times

    # Updates the time vector and measures observables.
    def _update(self, t: float):
        self.__times.append(self.__time)
        self.__time += t

    def evolve(self, t: float) -> None:
        raise NotImplementedError()

    def evolveFull(self, times: np.ndarray, callback, observables: List[Observable]) -> None:
        for i in range(1, len(times)):
            dt = times[i] - times[i-1]
            self.evolve(float(dt))
            callback(i, len(times))

            for o in observables:
                o.measure(self._psi, i, len(times))

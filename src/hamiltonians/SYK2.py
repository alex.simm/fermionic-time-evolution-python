from typing import List

import numpy as np

from src.Basis import Basis
from src.Log import Log
from src.State import State
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm
from src.hamiltonians.TermCallback import TermCallback


class SYK2(HamiltonianTerm):
    __N: int
    __J: float
    __coefficients: np.ndarray

    def __init__(self, N: int, J: float):
        super().__init__(False)
        self.__N = N
        self.__J = J
        self.__initCoefficients()

    def addEntries(self, callback: TermCallback, basis: Basis, bandSites: List[int]):
        scaling = 1.0 / np.sqrt(len(bandSites))
        states = basis.getStates()

        idx = 0
        for state1 in states:
            if len(basis.getStates()) > 100 and basis.getN() > 12 and (idx % 100) == 0:
                Log.debug(f"\tstate {idx} / {len(states)}")
            state1Idx = basis.getStateIndex(state1)

            # hopping terms
            for i in bandSites:
                for j in bandSites:
                    state2 = state1.applyTerm2(i, j)
                    if not state2.isZero():
                        callback.addEntry(scaling * self.__coefficients[i, j], state1, state2, state1Idx)

            # correction terms
            for i in bandSites:
                callback.addEntry(-0.5 * scaling * self.__coefficients[i, i], state1, state1, state1Idx)

    def update(self):
        self.__initCoefficients()

    def __str__(self):
        return "SYK q=2 (J=" + str(self.__J) + ")"

    def __initCoefficients(self):
        """
        Fills the coefficients array with random normal distributed values and makes it hermitean.
        :return:
        """
        norm = 1.0 / np.sqrt(2.0)
        self.__coefficients = np.zeros(shape=(self.__N, self.__N))
        for i in range(self.__N):
            for j in range(self.__N):
                if i != j:
                    self.__coefficients[i][j] = norm * np.random.normal(loc=0.0, scale=self.__J)
                    self.__coefficients[j][i] = np.conjugate(self.__coefficients[i][j])

import numpy as np

from src.Basis import Basis
from src.State import State
from src.observables.Observable import Observable


class Occupation(Observable):
    """
    Measures the occupation per site.
    """
    __basis: Basis

    def __init__(self, basis: Basis):
        super().__init__()
        self.__basis = basis

    def measure(self, psi: np.ndarray, step: int, numSteps: int):
        states = self.__basis.getStates()
        absSq = np.abs(psi) ** 2

        occupations = np.zeros(self.__basis.getN())
        for site in range(len(occupations)):
            for i in range(len(psi)):
                if states[i].isSiteOccupied(site):
                    occupations[site] += absSq[i]
        self._add(occupations)

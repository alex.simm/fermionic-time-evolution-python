from src.BlockDiagonalHamiltonian import BlockDiagonalHamiltonian


def testBasicProperties():
    for N in range(1, 12):
        H = BlockDiagonalHamiltonian(N, [])
        assert H.getBasis().getN() == N
        assert len(H.getBlocks()) == N + 1
        assert H.getBasis().dim() == 2 ** N

        blocks = H.getBlocks()
        for i in range(N + 1):
            assert blocks[i] == H.getBlock(i)
            assert blocks[i].getBasis().getQ() == i

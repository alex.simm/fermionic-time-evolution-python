from typing import List

import numpy as np

from src.Basis import Basis
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm
from src.hamiltonians.TermCallback import TermCallback


class Hopping(HamiltonianTerm):
    __hoppingEnergies: np.ndarray

    def __init__(self, hoppingEnergies: np.ndarray):
        super().__init__(True)
        self.__hoppingEnergies = hoppingEnergies

    def __str__(self):
        return "hopping"

    def addEntries(self, callback: TermCallback, basis: Basis, bandSites: List[int]):
        N = basis.getN()

        for state in basis.getStates():
            stateIdx = basis.getStateIndex(state)
            for i in bandSites:
                for j in bandSites:
                    state2 = state.applyTerm2(i, j)
                    if not state2.isZero():
                        callback.addEntry(float(self.__hoppingEnergies[i, j]) / N, state, state2, stateIdx)

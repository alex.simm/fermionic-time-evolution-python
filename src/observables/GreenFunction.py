from typing import List

import numpy as np

from src.Basis import Basis
from src.Hamiltonian import Hamiltonian
from src.HamiltonianBlock import HamiltonianBlock
from src.Log import Log
from src.State import State
from src.observables.Observable import Observable

# TODO: test
class GreenFunction(Observable):
    """
    Calculates the spectral function, Green's function and density of states of a Hamiltonian. The delta peaks are
    approximated by Lorentz functions of width eta.

    :param H block of a Hamiltonian with a fixed particle number
    :param Hplus block of the Hamiltonian with one more particle
    :param Hminus block of the Hamiltonian with one less particle
    :param eta width of the approximated delta peaks
    """
    __H: HamiltonianBlock
    __Hplus: HamiltonianBlock | None
    __Hminus: HamiltonianBlock | None
    __eta: float
    __groundStateEnergyTolerance: float
    # Creation and annihilation operators for each site. Will be empty if no lower or upper block exists.
    __creationOps: List[np.ndarray]
    __annihilationOps: List[np.ndarray]
    # Ground state energies and eigenstates (can be degenerate)
    __gsEnergies: np.ndarray
    __groundStates: np.ndarray

    def __init__(self, H: HamiltonianBlock, Hplus: HamiltonianBlock | None, Hminus: HamiltonianBlock | None, eta: float,
                 groundStateEnergyTolerance: float = 1e-10):
        self.__H = H
        self.__Hplus = Hplus
        self.__Hminus = Hminus
        self.__eta = eta
        self.__groundStateEnergyTolerance = groundStateEnergyTolerance

        if ((Hplus is not None and H.getBasis().getN() != Hplus.getBasis().getN())
                or (Hminus is not None and H.getBasis().getN() != Hminus.getBasis().getN())):
            raise RuntimeError("Hamiltonians need to have the same number of sites")

        # creation and annihilation operators for each site
        for site in range(H.getBasis().getN()):
            if Hplus is not None:
                self.__annihilationOps.append(self.__calculateMatrix(Hplus.getBasis(), H.getBasis(), site, True))
            if Hminus is not None:
                self.__creationOps.append(self.__calculateMatrix(Hminus.getBasis(), H.getBasis(), site, False))

        # find all ground states
        evals = H.getEigenvalues()
        evecs = H.getEigenvectors()
        self.__gsEnergies = evals[0:1]
        self.__groundStates = evecs[:, 0]

        idx = 0
        while idx < evals.size() and np.abs(evals[idx] - self.__gsEnergies[0]) < groundStateEnergyTolerance:
            self.__gsEnergies = np.append(self.__gsEnergies, evals[idx])
            self.__groundStates = np.append(self.__groundStates, evecs[:, idx], axis=1)
            idx = idx + 1

    def measure(self, psi: np.ndarray, step: int, numSteps: int):
        pass

    def numGroundStates(self) -> int:
        return self.__gsEnergies.shape[0]

    # TODO: improve, vectorise
    def spectralFunction(self, omega: float, site: int) -> float:
        """
        Calculates the on-site spectral function -1/pi * Im(G^R) in spectral representaton for a specific energy omega.
        """
        summed = 0.0

        if len(self.__creationOps) > 0:
            op = self.__creationOps[site]
            evalsMinus = self.__Hminus.getEigenvalues()
            evecsMinus = self.__Hminus.getEigenvectors()

            # iterate all ground states and all states with one particle less
            for m in range(len(evalsMinus)):
                privateSum = 0
                created = op @ evecsMinus[:, m]

                for n in range(len(self.__gsEnergies)):
                    product = np.abs(np.vdot(self.__groundStates[n], created)) ** 2
                    summed += product * self.__delta(omega - self.__gsEnergies[n] + evalsMinus[n], self.__eta)

        if len(self.__annihilationOps) > 0:
            op = self.__annihilationOps[site]
            evalsPlus = self.__Hplus.getEigenvalues()
            evecsPlus = self.__Hplus.getEigenvectors()

            # iterate all ground states and all states with one particle more
            for m in range(len(evalsPlus)):
                privateSum = 0
                annihilated = op @ evecsPlus[:, m]

                for n in range(len(self.__gsEnergies)):
                    product = np.abs(np.vdot(self.__groundStates[n], annihilated)) ** 2
                    summed += product * self.__delta(omega + self.__gsEnergies[n] - evalsPlus[n], self.__eta)

        return summed / (self.__gsEnergies.shape[0] * self.__H.getBasis().getN())

    # TODO: improve, vectorise
    def spectralFunctionRange(self, omegas: np.ndarray, site: int) -> np.ndarray:
        """
        Calculates the on-site spectral function -1/pi * Im(G^R) in spectral representaton for a range of energies omegas.
        """
        summed = np.zeros(omegas.shape[0])

        if len(self.__creationOps) > 0:
            op = self.__creationOps[site]
            evalsMinus = self.__Hminus.getEigenvalues()
            evecsMinus = self.__Hminus.getEigenvectors()

            # iterate all ground states (n) and all states with one particle less (m)
            for m in range(len(evalsMinus)):
                created = op @ evecsMinus[:, m]

                for n in range(len(self.__gsEnergies)):
                    product = np.abs(np.vdot(self.__groundStates[n], created)) ** 2
                    Ediff = self.__gsEnergies[n] - evalsMinus[m]

                    if product != 0:
                        for i in range(len(omegas)):
                            delta = self.__delta(omegas[i] - Ediff, self.__eta)
                            summed[i] += product * delta

        if len(self.__annihilationOps) > 0:
            op = self.__annihilationOps[site]
            evalsPlus = self.__Hplus.getEigenvalues()
            evecsPlus = self.__Hplus.getEigenvectors()

            # iterate all ground states (n) and all states with one particle more (m)
            for m in range(len(evalsPlus)):
                annihilated = op @ evecsPlus[:, m]

                for n in range(len(self.__gsEnergies)):
                    product = np.abs(np.vdot(self.__groundStates[n], annihilated)) ** 2
                    Ediff = self.__gsEnergies[n] - evalsPlus[m]

                    if product != 0:
                        for i in range(len(omegas)):
                            delta = self.__delta(omegas[i] + Ediff, self.__eta)
                            summed[i] += product * delta

        return summed / (self.__gsEnergies.shape[0] * self.__H.getBasis().getN())

    def densityOfStates(self, omega: float) -> float:
        """
        Calculates the density of states (trace of spectral function) for a specific energy omega.
        """
        trace = 0
        for i in range(self.__H.getBasis().getN()):
            Log.debug(f"spectral function of site {i}")
            trace += self.spectralFunction(omega, i)
        return trace

    def densityOfStatesRange(self, omegas: np.ndarray) -> np.ndarray:
        """
        Calculates the density of states (trace of spectral function) for a range of energies omegas.
        """
        trace = np.zeros(omegas.shape[0])
        for i in range(self.__H.getBasis().getN()):
            Log.debug(f"spectral function of site {i}")
            trace += self.spectralFunctionRange(omegas, i)
        return trace

    @staticmethod
    def __calculateMatrix(src: Basis, dst: Basis, site: int, annihilation: bool) -> np.ndarray:
        """
        Computes the matrix that has entries (state coefficients) for all possible conversions of states in Hsrc into
        states in Hdst by creating or annihilating a particle at the given site.
        """
        statesSrc = src.getStates()
        statesDst = dst.getStates()

        c = np.zeros(shape=(len(statesDst), len(statesSrc)))
        for state in statesSrc:
            if state.isSiteOccupied(site):
                idx = src.getStateIndex(state)

                state2 = state.copy()
                if annihilation:
                    state2.annihilate(site)
                else:
                    state2.create(site)

                if not state2.isZero():
                    c[dst.getStateIndex(state2), idx] = float(state2.getCoefficient())
        return c

    @staticmethod
    def __delta(x: float, eta: float) -> float:
        """
        Approximates the delta distribution at point x by a Lorentzian with a tiny (infinitesimal) eta.
        :param x: eta width of the Lorentzian
        :param eta: x parameter of the delta
        """
        return eta / ((x * x + eta * eta) * np.pi)

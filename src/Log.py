class Log:
    LEVEL_NONE = 0
    LEVEL_INFO = 1
    LEVEL_DEBUG = 2

    __level: int

    def __init__(self, level: int = LEVEL_INFO):
        self.__level = level

    @staticmethod
    def getInstance():
        return Log()

    def getLevel(self) -> int:
        return self.__level

    def setLevel(self, level: int) -> None:
        self.__level = level

    @staticmethod
    def info(s: str) -> None:
        Log.getInstance().log(Log.LEVEL_INFO, s)

    @staticmethod
    def debug(s: str) -> None:
        Log.getInstance().log(Log.LEVEL_DEBUG, s)

    def log(self, level: int, s: str) -> None:
        if level <= self.__level:
            print(s)

import numpy as np
import numpy.testing as testing
import pytest

from src.HamiltonianBlock import HamiltonianBlock
from src.evolution.ExactEvolution import ExactEvolution
from test.hamiltonian.DiagonalHamiltonianTerm import DiagonalHamiltonianTerm


@pytest.fixture
def randomDiagonalHamiltonianBlock():
    def _method(N: int, Q: int):
        energies = np.random.rand(N)
        diagonal = DiagonalHamiltonianTerm(energies)
        return HamiltonianBlock(N, Q, [diagonal])

    return _method


def testAutoDiagonalisation(randomDiagonalHamiltonianBlock):
    evals = np.random.rand(10)
    diagonal = DiagonalHamiltonianTerm(evals)
    H = HamiltonianBlock(len(evals), 1, [diagonal])

    sim = ExactEvolution(H, True)
    assert not H.isDiagonalised()
    sim.evolve(0.0)
    assert H.isDiagonalised()

    sortedEvals = np.sort(evals)
    testing.assert_array_almost_equal(H.getEigenvalues(), sortedEvals, decimal=5)


def testExactEvolutionWithDiagonalHamiltonian():
    evals = np.array([1, 1, 1])
    diagonal = DiagonalHamiltonianTerm(evals)
    H = HamiltonianBlock(len(evals), 1, [diagonal])

    sim = ExactEvolution(H, True)
    sim.setPsiState(H.getBasis().getState(0))

    sim.evolve(0)
    testing.assert_array_almost_equal(sim.getPsi(), np.array([1.0, 0.0, 0.0]), decimal=5)
    sim.evolve(np.pi / 2.0)
    testing.assert_array_almost_equal(sim.getPsi(), np.array([-1.0j, 0.0, 0.0]), decimal=5)
    sim.evolve(-np.pi - np.pi / 4)
    testing.assert_array_almost_equal(sim.getPsi(), np.array([(1.0j - 1.0) / np.sqrt(2.0), 0.0, 0.0]), decimal=5)

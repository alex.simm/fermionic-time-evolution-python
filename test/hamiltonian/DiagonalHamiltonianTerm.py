from typing import Set

import numpy as np

from src.Basis import Basis
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm
from src.hamiltonians.TermCallback import TermCallback


class DiagonalHamiltonianTerm(HamiltonianTerm):
    """
    Utility class that represents a diagonal Hamiltonian with fixed eigenvalues.
    """
    __eigenvalues: np.ndarray

    def __init__(self, eigenvalues: np.ndarray):
        super().__init__(True)
        self.__eigenvalues = eigenvalues

    def addEntries(self, callback: TermCallback, basis: Basis, bandSites: Set[int]):
        for state in basis.getStates():
            stateIdx = basis.getStateIndex(state)
            callback.addEntry2(float(self.__eigenvalues[stateIdx]), state, state, stateIdx, stateIdx)

    def __str__(self):
        return np.array2string(self.__eigenvalues)

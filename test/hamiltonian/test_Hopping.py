import numpy as np
import numpy.testing as testing
import pytest

from src.HamiltonianBlock import HamiltonianBlock
from src.hamiltonians.ChemicalPotential import ChemicalPotential
from src.hamiltonians.Hopping import Hopping


@pytest.fixture
def randomHoppingTerm():
    def _method(N: int):
        hoppingEnergies = np.random.rand(N, N)
        hoppingEnergies += np.conjugate(hoppingEnergies).T
        np.fill_diagonal(hoppingEnergies, 0.0)
        return Hopping(hoppingEnergies)

    return _method


def testHamiltonianIsHermitian(randomHoppingTerm):
    term = randomHoppingTerm(8)
    H = HamiltonianBlock(8, 4, [term])
    m = H.asMatrix()
    testing.assert_array_almost_equal(np.conjugate(m).T, m)


def testRealEigenvalues(randomHoppingTerm):
    term = randomHoppingTerm(8)
    H = HamiltonianBlock(8, 4, [term])
    H.diagonalise()
    evals = H.getEigenvalues()

    testing.assert_array_almost_equal(np.imag(evals), np.zeros(len(evals)), decimal=5)

from typing import List

from src.Basis import Basis
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm
from src.hamiltonians.TermCallback import TermCallback


class ChemicalPotential(HamiltonianTerm):
    __mu: float

    def __init__(self, mu: float):
        super().__init__(True)
        self.__mu = mu

    def getMu(self) -> float:
        return self.__mu

    def setMu(self, mu: float):
        self.__mu = mu

    def __str__(self):
        return f"Chemical potential (mu={self.__mu})"

    def addEntries(self, callback: TermCallback, basis: Basis, bandSites: List[int]):
        states = basis.getStates()
        for state in states:
            stateIdx = basis.getStateIndex(state)
            num = state.numOccupiedSitesList(bandSites)
            callback.addEntry2(-self.__mu * num, state, state, stateIdx, stateIdx)

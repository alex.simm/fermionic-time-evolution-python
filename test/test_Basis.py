import numpy.testing as testing

from src.Basis import Basis
from src.State import State


def testSingleParticleStates():
    b = Basis(5, 1)
    assert (b.getN() == 5)
    assert (b.getQ() == 1)
    assert (b.dim() == 5)
    assert set([state.stateNumber() for state in b.getStates()]) == {1, 2, 4, 8, 16}


def testManyParticleStates():
    b1 = Basis(4, 2)
    assert b1.getN() == 4
    assert b1.getQ() == 2
    assert set([state.stateNumber() for state in b1.getStates()]) == {3, 5, 6, 9, 10, 12}

    b2 = Basis(10, 7)
    assert b2.getN() == 10
    assert b2.getQ() == 7
    assert (set([state.stateNumber() for state in b2.getStates()]) ==
            {127, 191, 223, 239, 247,
             251, 253, 254, 319, 351,
             367, 375, 379, 381, 382,
             415, 431, 439, 443, 445,
             446, 463, 471, 475, 477,
             478, 487, 491, 493, 494,
             499, 501, 502, 505, 506,
             508, 575, 607, 623, 631,
             635, 637, 638, 671, 687,
             695, 699, 701, 702, 719,
             727, 731, 733, 734, 743,
             747, 749, 750, 755, 757,
             758, 761, 762, 764, 799,
             815, 823, 827, 829, 830,
             847, 855, 859, 861, 862,
             871, 875, 877, 878, 883,
             885, 886, 889, 890, 892,
             911, 919, 923, 925, 926,
             935, 939, 941, 942, 947,
             949, 950, 953, 954, 956,
             967, 971, 973, 974, 979,
             981, 982, 985, 986, 988,
             995, 997, 998, 1001,
             1002,
             1004, 1009, 1010, 1012,
             1016})


def testVariableNumberBasis():
    for N in range(2, 10):
        b = Basis(N, -1)
        assert b.getN() == N
        assert b.getQ() == -1
        assert b.dim() == 2 ** N


def testGetState():
    b = Basis(4, 4)

    assert b.getState(0) == State(4, 15)
    testing.assert_raises(RuntimeError, b.getState, 1)

    evals2 = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
    b2 = Basis(8, 4)
    states = b2.getStates()
    for i in range(len(states)):
        assert b2.getState(i) == states[i]
    for i in range(len(states), len(states) + 10):
        testing.assert_raises(RuntimeError, b.getState, i)


def testStateIndex():
    b = Basis(4, 4)
    assert b.getStateIndex(State(4, 15)) == 0
    assert b.getStateIndex(State(4, 13)) == -1

    b2 = Basis(8, 4)
    states = b2.getStates()
    for i in range(len(states)):
        assert b2.getStateIndex(states[i]) == i

    # a zero-state should not have a valid index, even if it is in the state space
    state = b2.getState(0)
    state.create(5)
    state.annihilate(0)
    state.annihilate(0)
    assert state.isZero()
    assert b2.getStateIndex(state) == -1

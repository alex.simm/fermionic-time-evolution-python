import numpy as np
import numpy.testing as testing

from src.HamiltonianBlock import HamiltonianBlock
from src.hamiltonians.OnSiteEnergy import OnSiteEnergy


def testHamiltonianIsHermitian():
    term = OnSiteEnergy(np.linspace(-1, 1, 8))
    H = HamiltonianBlock(8, 4, [term])
    m = H.asMatrix()
    testing.assert_array_almost_equal(m, np.conjugate(m.T))


def testRealEigenvalues():
    term = OnSiteEnergy(np.linspace(-1, 1, 8))
    H = HamiltonianBlock(8, 4, [term])
    H.diagonalise()
    evals = H.getEigenvalues()

    testing.assert_array_almost_equal(np.imag(evals), np.zeros(len(evals)), decimal=5)


def testOrthonormalEigenvectors():
    onsite = OnSiteEnergy(np.linspace(1.0, 8.0, 8))
    H = HamiltonianBlock(8, 4, [onsite])
    H.diagonalise()
    evecs = H.getEigenvectors()

    for i in range(H.getBasis().dim()):
        testing.assert_almost_equal(np.vdot(evecs[:, i], evecs[:, i]), 1.0)

        for j in range(i):
            testing.assert_approx_equal(np.vdot(evecs[:, i], evecs[:, j]), 0)
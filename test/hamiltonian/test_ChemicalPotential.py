import numpy as np
import numpy.testing as testing

from src.HamiltonianBlock import HamiltonianBlock
from src.hamiltonians.ChemicalPotential import ChemicalPotential


def testHamiltonianIsHermitian():
    term = ChemicalPotential(1)
    H = HamiltonianBlock(8, 4, [term])
    m = H.asMatrix()
    testing.assert_array_almost_equal(np.conjugate(m).T, m)


def testRealEigenvalues():
    term = ChemicalPotential(1)
    H = HamiltonianBlock(8, 4, [term])
    H.diagonalise()
    evals = H.getEigenvalues()

    testing.assert_array_almost_equal(np.imag(evals), np.zeros(len(evals)), decimal=5)

from typing import List, Tuple

import numpy as np
import scipy.sparse as sparse
import scipy.sparse.linalg as sparselin

from src.Basis import Basis
from src.Hamiltonian import Hamiltonian
from src.Log import Log
from src.State import State
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm
from src.hamiltonians.TermCallback import TermCallback


class HamiltonianBlock(Hamiltonian, TermCallback):
    __MIN_DIAGONALISATION_EIGENVALUES = 3
    __MIN_DIAGONALISATION_EIGENVECTORS = 3

    __terms: List[HamiltonianTerm]
    """ Whether the Hamiltonian only contains real values. """
    __realMatrix: bool
    """ Whether the matrix is sparse. """
    __sparse: bool
    """ Basis of the Hilbert space. """
    __basis: Basis
    """ Matrix representation """
    __H: np.ndarray | None = None
    __Hsparse: np.ndarray | None = None
    """ Eigenvalues and eigenvectors """
    __eigenvalues: np.ndarray | None = None
    __eigenvectors: np.ndarray | None = None

    def __init__(self, N: int, Q: int, terms: List[HamiltonianTerm], sparse: bool = False):
        """ Constructs a Hamiltonian block with N sites and Q particles without impurity. """
        super().__init__(N, terms)
        self.__basis = Basis(N, Q)
        self.__sparse = sparse
        self.__terms = terms

        self.__realMatrix = True
        for term in terms:
            if not term.isReal():
                self.__realMatrix = False
        self.updateMatrix(False)

    def getBasis(self) -> Basis:
        return self.__basis

    def isUsingSparseMatrix(self) -> bool:
        return self.__sparse

    def getSparseness(self) -> float:
        dim = self.__basis.dim()
        if self.__sparse:
            num = self.__Hsparse.count_nonzero()
        else:
            num = (np.abs(self.__H) > 1e-10).sum()
        return num / (dim * dim)

    def asMatrix(self) -> np.array:
        return self.__H

    def asSparseMatrix(self) -> np.array:
        return self.__Hsparse

    def isDiagonalised(self) -> bool:
        return self.__eigenvalues is not None

    def diagonalise(self, includeEigenvectors: bool = True, onlyGroundState: bool = False):
        if self.__sparse:
            if onlyGroundState:
                self.__eigenvalues = np.zeros(1)
                evals, evecs = self.__diagonaliseSparse(2, False, includeEigenvectors)
                self.__eigenvalues[0] = evals[0]
                if includeEigenvectors:
                    self.__eigenvectors = evecs[:, 0:1]
            else:
                dim = self.__basis.dim()
                half = (dim + 1) // 2
                self.__eigenvalues = np.zeros(dim)
                self.__eigenvectors = np.zeros((dim, dim)) if includeEigenvectors else None

                evals, evecs = self.__diagonaliseSparse(half, True, includeEigenvectors)
                self.__eigenvalues[0:half] = evals
                if includeEigenvectors:
                    self.__eigenvectors[:, 0:half] = evecs

                evals, evecs = self.__diagonaliseSparse(half, False, includeEigenvectors)
                self.__eigenvalues[-half:] = evals
                if includeEigenvectors:
                    self.__eigenvectors[:, -half:] = evecs
                self.__eigenvalues = np.sort(self.__eigenvalues)
        else:
            if includeEigenvectors:
                self.__eigenvalues, self.__eigenvectors = np.linalg.eigh(self.__H)
            else:
                self.__eigenvalues = np.linalg.eigvalsh(self.__H)
                self.__eigenvectors = None

    def getEigenvalues(self) -> np.array:
        if not self.isDiagonalised():
            raise RuntimeError("Hamiltonian is not diagonalised")
        return self.__eigenvalues

    def getEigenvectors(self) -> np.array:
        if not self.isDiagonalised():
            raise RuntimeError("Hamiltonian is not diagonalised")
        return self.__eigenvectors

    # ==================== TermCallback interface =====================
    def addEntry(self, entry: complex, state1: State, state2: State, state1Index: int) -> None:
        """ Adds a real term to the Hamiltonian if both states exist. """
        state2Index = self.__basis.getStateIndex(state2)
        self.addEntry2(entry, state1, state2, state1Index, state2Index)

    def addEntry2(self, entry: complex, state1: State, state2: State, state1Index: int, state2Index: int) -> None:
        """ Adds a real term to the Hamiltonian if both states exist. """
        if (not state1.isZero()) and (not state2.isZero()):
            d = self.__basis.dim()
            if state1Index < 0 or state1Index >= d:
                raise RuntimeError("Illegal state index occured: " + str(state1Index))
            if state2Index < 0 or state2Index >= d:
                raise RuntimeError("Illegal state index occured: " + str(state2Index))

            if self.__sparse:
                self.__Hsparse[state2Index, state1Index] += entry * state2.coefficient()
            else:
                self.__H[state2Index, state1Index] += entry * state2.coefficient()

    def updateMatrix(self, updateTerms: bool = False):
        if updateTerms:
            for term in self.__terms:
                term.update()

        # create a new matrix
        dim = self.__basis.dim()
        if self.__sparse:
            self.__Hsparse = sparse.csr_matrix((dim, dim), dtype=np.complex64)
            self.__H = None
        else:
            self.__H = np.zeros(shape=(dim, dim))
            self.__Hsparse = None

        # fill it by calling all terms
        bandSites = self.getSites()
        for term in self.__terms:
            term.addEntries(self, self.__basis, bandSites)

    def __diagonaliseSparse(self, num: int, highest: bool, includeEigenvectors: bool) -> Tuple[np.ndarray, np.ndarray]:
        """ Finds the highest or lowest eigenvalues of the sparse Hamiltonian. """
        numValues = max(num + 1, 3)
        realStr = "real" if self.__realMatrix else "complex"
        Log.debug(f"find sparse eigenvalues ({realStr}): {numValues}")

        which = 'LM' if highest else 'SM'
        evals, evecs = sparselin.eigsh(self.__Hsparse, numValues, which=which, return_eigenvectors=includeEigenvectors)

        evals = evals[-num:] if highest else evals[0:num]
        if includeEigenvectors:
            evecs = evecs[:, -num:] if highest else evecs[:, 0:num]
        return evals, evecs

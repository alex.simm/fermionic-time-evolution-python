import numpy as np

from src.Hamiltonian import Hamiltonian
from src.observables.Observable import Observable


class EnergyObservable(Observable):
    """
    Measures the total energy of the system, i.e. the expectation value of the Hamiltonian.
    """
    __H: Hamiltonian

    def __init__(self, H: Hamiltonian):
        super().__init__()
        self.__H = H

    def measure(self, psi: np.ndarray, step: int, numSteps: int):
        if self.__H.isDiagonalised():
            evals = self.__H.getEigenvalues()
            evecs = self.__H.getEigenvectors()

            E = 0
            for i in range(self.__H.getBasis().dim()):
                E += evals[i] * np.abs(np.vdot(psi, evecs[:, i])) ** 2
        else:
            if self.__H.isUsingSparseMatrix():
                E = np.vdot(psi, self.__H.asSparseMatrix() * psi).real
            else:
                E = np.vdot(psi, self.__H.asMatrix() * psi).real

        self._add(E)

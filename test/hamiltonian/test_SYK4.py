import numpy as np
import numpy.testing as testing

from src.HamiltonianBlock import HamiltonianBlock
from src.hamiltonians.SYK4 import SYK4


def testHamiltonianIsHermitian():
    for N in range(2, 8):
        for Q in range(N + 1):
            J = np.random.rand() * 3
            term = SYK4(N, J)
            H = HamiltonianBlock(N, Q, [term])
            m = H.asMatrix()
            testing.assert_array_almost_equal(np.conjugate(m).T, m)


def testRealEigenvalues():
    for N in range(2, 8):
        for Q in range(N + 1):
            J = np.random.rand() * 3
            term = SYK4(N, J)
            H = HamiltonianBlock(N, Q, [term])
            H.diagonalise()
            evals = H.getEigenvalues()
            testing.assert_array_almost_equal(np.imag(evals), np.zeros(len(evals)), decimal=5)

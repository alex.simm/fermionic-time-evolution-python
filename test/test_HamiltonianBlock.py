from src.HamiltonianBlock import HamiltonianBlock
import numpy.testing as testing


def testDiagonalisation():
    H = HamiltonianBlock(8, 4, [])
    assert not H.isDiagonalised()
    testing.assert_raises(RuntimeError, H.getEigenvalues)
    testing.assert_raises(RuntimeError, H.getEigenvectors)
    H.diagonalise()
    assert H.isDiagonalised()
    assert len(H.getEigenvalues()) == H.getBasis().dim()

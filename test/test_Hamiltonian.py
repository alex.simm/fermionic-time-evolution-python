import numpy as np
import numpy.testing as testing
import pytest

from src.HamiltonianBlock import HamiltonianBlock
from test.hamiltonian.DiagonalHamiltonianTerm import DiagonalHamiltonianTerm


@pytest.fixture
def diagonalHamiltonianBlock():
    def _method(N: int, Q: int, energyOffset=0.0):
        energies = np.arange(float(N)) + energyOffset
        diagonal = DiagonalHamiltonianTerm(energies)
        return HamiltonianBlock(N, Q, [diagonal])

    return _method


def testPartitionFunction(diagonalHamiltonianBlock):
    H = diagonalHamiltonianBlock(4, 1, energyOffset=1.0)
    H.diagonalise()
    testing.assert_approx_equal(H.partitionFunction(1), 0.57131743)
    testing.assert_approx_equal(H.partitionFunction(0.5), 1.332875544)

    H2 = diagonalHamiltonianBlock(5, 1, energyOffset=-2.0)
    H2.diagonalise()
    testing.assert_approx_equal(H2.partitionFunction(1), 11.61055265179775)
    testing.assert_approx_equal(H2.partitionFunction(0.5), 6.34141320004324)


def testAverageEnergy(diagonalHamiltonianBlock):
    H = diagonalHamiltonianBlock(4, 1, energyOffset=1.0)
    H.diagonalise()
    testing.assert_approx_equal(H.averageEnergy(1), 1.50734726541)
    testing.assert_approx_equal(H.averageEnergy(0.5), 1.9154235115381)

    H2 = diagonalHamiltonianBlock(5, 1, energyOffset=-2.0)
    H2.diagonalise()
    testing.assert_approx_equal(H2.averageEnergy(1), -1.451941567662)
    testing.assert_approx_equal(H2.averageEnergy(0.5), -0.9056333666324617)


def testFreeEnergy(diagonalHamiltonianBlock):
    H = diagonalHamiltonianBlock(4, 1, energyOffset=1.0)
    H.diagonalise()

    # partition function Z: 0.571317431664653136762583702
    # average energy E: 1.5073472654142302326293824756
    testing.assert_approx_equal(H.freeEnergy(1), 0.55981030143880)
    testing.assert_approx_equal(H.entropy(1.0), 0.94753696397542556312)
    # Z: 1.332875544269118266026603, E: 1.9154235115381356768587813
    testing.assert_approx_equal(H.freeEnergy(0.5), -0.5746773433966590298)
    testing.assert_approx_equal(H.entropy(0.5), 1.2450504274673973533293)

    H2 = diagonalHamiltonianBlock(5, 1, energyOffset=-2.0)
    H2.diagonalise()

    # Z: 11.6105526517977504760, E: -1.45194156766219473111
    testing.assert_approx_equal(H2.freeEnergy(1), -2.4519143959375933)
    testing.assert_approx_equal(H2.entropy(1), 0.99997282827539826889)
    # Z: 6.34141320004324912740, E: -0.90563336663246178279
    testing.assert_approx_equal(H2.freeEnergy(0.5), -3.69420329165030078144)
    testing.assert_approx_equal(H2.entropy(0.5), 1.394284962508919499325)

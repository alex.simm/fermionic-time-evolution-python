from typing import List

import numpy as np

from src.Basis import Basis
from src.Log import Log
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm
from src.hamiltonians.TermCallback import TermCallback


class OnSiteEnergy(HamiltonianTerm):
    __energies: np.ndarray

    def __init__(self, energies: np.ndarray):
        super().__init__(True)
        self.__energies = energies

    def __str__(self):
        s = np.array2string(self.__energies)
        return f"on-site energy ({s})"

    def getEnergies(self):
        return self.__energies

    def addEntries(self, callback: TermCallback, basis: Basis, bandSites: List[int]):
        states = basis.getStates()
        idx = 0

        for state in states:
            if len(basis.getStates()) > 100 and basis.getN() > 12 and (idx% 100) == 0:
                Log.debug(f"\tstate {idx} / {len(states)}")
                stateIdx = basis.getStateIndex(state)

                for i in bandSites:
                    if state.isSiteOccupied(i):
                        callback.addEntry2(float(self.__energies[i]), state, state, stateIdx, stateIdx)

from src.Hamiltonian import Hamiltonian
from src.evolution.TimeEvolution import TimeEvolution


class IdentityTimeEvolution(TimeEvolution):

    def __init__(self, system: Hamiltonian):
        super().__init__(system)

    def evolve(self, t: float):
        pass

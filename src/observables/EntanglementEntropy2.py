from typing import Set

import numpy as np
from qutip import Qobj, entropy_linear

from src.Basis import Basis
from src.State import State
from src.observables.Observable import Observable


# TODO: test
class EntanglementEntropy2(Observable):
    __fullBasis: Basis
    __fullBasisGrand: Basis
    __sitesA: Set[int]
    __sitesB: Set[int]
    __basisA: Basis
    __basisB: Basis
    __basisAreduced: Basis
    __basisBreduced: Basis

    def __init__(self, fullBasis: Basis, sitesA: Set[int]):
        super().__init__()
        self.__fullBasis = fullBasis
        self.__fullBasisGrand = Basis(fullBasis.getN(), -1)
        self.__sitesA = sitesA
        self.__sitesB = fullBasis.getSites() - sitesA
        self.__basisA = Basis(len(sitesA), -1)
        self.__basisB = Basis(len(self.__sitesB), -1)

    def measure(self, psi: np.ndarray, step: int, numSteps: int):
        dim = len(psi)
        dimA = self.__basisA.dim()
        dimB = self.__basisB.dim()

        # scale up psi to grand-canonical
        psi2 = np.zeros(self.__fullBasisGrand.dim())
        for i in range(dim):
            state = self.__fullBasis.getState(i)
            index = self.__fullBasisGrand.getStateIndex(state)
            psi2[index] = psi[i]

        # density matrix of pure state
        rho = np.kron(psi2, psi2.T).reshape((len(psi2), len(psi2)))

        # convert to Qutip
        rhoQ = Qobj(rho, dims=[[dimA, dimB], [dimA, dimB]])
        #rhoQ = Qobj(rho, dims=[dim, dim])

        # partial trace
        rhoA = rhoQ.ptrace(0)

        # entropy
        entropy = entropy_linear(rhoA)
        self._add(entropy)

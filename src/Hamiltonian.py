from typing import List, Set

import numpy as np

from src.Basis import Basis
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm


class Hamiltonian:
    """
    A Hamiltonian contains multiple terms (instances of HamiltonianTerm) and an optional impurity.
    """
    """ Index of the impurity. Value of -1 means no impurity. """
    __impurityIndex: int
    """ Indices of the band (non-impurity) sites """
    __bandSites: List[int]
    """ List of all terms. """
    __terms: List[HamiltonianTerm]

    def __init__(self, N: int, terms: List[HamiltonianTerm]):
        """ Creates a Hamiltonian for N sites without impurity. """
        self.__terms = terms
        self.__bandSites = list(range(N))

    def getTerms(self) -> List[HamiltonianTerm]:
        """ Returns a list of all terms in this Hamiltonian. """
        return self.__terms

    def getBasis(self) -> Basis:
        """
        Returns the basis of the Hilbert space on which this Hamiltonian is defined. The index of each basis state
        corresponds to the index in the state vector.
        """
        raise NotImplementedError()

    def getSites(self) -> List[int]:
        return self.__bandSites

    def isUsingSparseMatrix(self) -> bool:
        """
        Returns whether the Hamiltonian uses sparse matrices. This determines whether asMatrix() or
        asSparseMatrix() return a valid matrix representation.
        """
        raise NotImplementedError()

    def asMatrix(self) -> np.array:
        """ Return the full matrix form of the Hamiltonian. """
        raise NotImplementedError()

    def asSparseMatrix(self) -> np.array:
        """ Return the sparse matrix form of the Hamiltonian, if it is using one. """
        raise NotImplementedError()

    def isDiagonalised(self) -> bool:
        """ Returns whether this Hamiltonian was already diagonalised. """
        raise NotImplementedError()

    def diagonalise(self, includeEigenvectors: bool = True, onlyGroundState: bool = False):
        """
        Diagonalises thie Hamiltonian and stores the resulting eigenvalues and eigenvectors.

        :param includeEigenvectors: whether the eigenvectors should be calculated, too. If false, only eigenvalues will
         be calculated and stored.
        :param onlyGroundState: if only the ground state or all eigenstates should be found
        """
        raise NotImplementedError()

    def getEigenvalues(self) -> np.array:
        """ Returns the eigenvalues of this Hamiltonian, if it was diagonalised. """
        raise NotImplementedError()

    def getEigenvectors(self) -> np.array:
        """ Returns the eigenvectors of this Hamiltonian as the columns of a matrix, if it was diagonalised. """
        raise NotImplementedError()

    def getSparseness(self) -> float:
        """ Returns the percentage of non-null entries in this HamiltonianBlock. """
        raise NotImplementedError()

    def updateMatrix(self, updateTerms: bool):
        """
        Reinitialises and fills the matrix and, optionally, updates all terms are before. If updated, all terms
        that contain random numbers will be filled with new numbers. This needs to be done after modifying any
        of the terms' parameters and clears the diagonalisation status and all eigenvalues and eigenvectors.
        """
        raise NotImplementedError()

    def partitionFunction(self, beta: float):
        if not self.isDiagonalised():
            raise RuntimeError("Hamiltonian must be diagonalised for thermodynamic properties")
        evals = self.getEigenvalues()

        return np.sum(np.exp(-beta * evals))

    def averageEnergy(self, beta: float):
        if not self.isDiagonalised():
            raise RuntimeError("Hamiltonian must be diagonalised for thermodynamic properties")
        evals = self.getEigenvalues()

        boltzmannWeights = np.exp(-beta * evals)
        return np.sum(evals * boltzmannWeights) / self.partitionFunction(beta)

    def freeEnergy(self, beta: float):
        return -np.log(self.partitionFunction(beta)) / beta

    def entropy(self, beta: float):
        return beta * (self.averageEnergy(beta) - self.freeEnergy(beta))

import numpy as np

from src.Hamiltonian import Hamiltonian
from src.evolution.TimeEvolution import TimeEvolution


class ExactEvolution(TimeEvolution):
    __autoDiagonalise: bool

    def __init__(self, system: Hamiltonian, autoDiagonalise: bool = False):
        super().__init__(system)
        self.__autoDiagonalise = autoDiagonalise
        self.__psiTmp = np.empty(system.getBasis().dim(), dtype=np.complex64)

    def evolve(self, t: float) -> None:
        if not self._system.isDiagonalised():
            if self.__autoDiagonalise:
                self._system.diagonalise()
            else:
                raise RuntimeError("System is not diagonalised")

        psiTmp = np.zeros_like(self._psi, dtype=np.complex64)
        evals = self._system.getEigenvalues()
        evecs = self._system.getEigenvectors()
        evalsExp = np.exp(-1.0j * t * evals)
        for n in range(len(evals)):
            amplitude = np.vdot(evecs[:, n], self._psi)
            psiTmp += amplitude * evalsExp[n] * evecs[:, n]

        self._psi = psiTmp
        self._update(t)

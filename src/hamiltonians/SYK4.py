from typing import List

import numpy as np

from src.Basis import Basis
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm
from src.hamiltonians.TermCallback import TermCallback


class SYK4(HamiltonianTerm):
    __N: int
    __J: float
    __coefficients: np.ndarray

    def __init__(self, N: int, J: float):
        super().__init__(False)
        self.__N = N
        self.__J = J
        self.__initCoefficients()

    def addEntries(self, callback: TermCallback, basis: Basis, bandSites: List[int]):
        scaling = 1.0 / np.power(2.0 * len(bandSites), 1.5)
        states = basis.getStates()

        idx = 0
        for state1 in states:
            state1Idx = basis.getStateIndex(state1)

            # main terms
            for i in bandSites:
                for j in bandSites[0:i]:
                    for k in bandSites:
                        for l in bandSites[0:k]:
                            state2 = state1.applyTerm4(i, j, k, l)
                            if not state2.isZero():
                                state2Idx = basis.getStateIndex(state2)
                                callback.addEntry2(self.__coefficients[i, j, k, l] * scaling, state1, state2, state1Idx,
                                                   state2Idx)
                                callback.addEntry2(-self.__coefficients[j, i, k, l] * scaling, state1, state2,
                                                   state1Idx, state2Idx)
                                callback.addEntry2(-self.__coefficients[i, j, l, k] * scaling, state1, state2,
                                                   state1Idx, state2Idx)
                                callback.addEntry2(self.__coefficients[j, i, l, k] * scaling, state1, state2, state1Idx,
                                                   state2Idx)

            # correction terms
            for i in bandSites:
                for j in bandSites:
                    for l in bandSites:
                        state2 = state1.applyTerm2(j, l)
                        callback.addEntry(0.5 * self.__coefficients[i, j, i, l] * scaling, state1, state2, state1Idx)
                        state2 = state1.applyTerm2(i, l)
                        callback.addEntry(-0.5 * self.__coefficients[i, j, j, l] * scaling, state1, state2, state1Idx)
            for i in bandSites:
                for j in bandSites:
                    for k in bandSites:
                        state2 = state1.applyTerm2(i, k)
                        callback.addEntry(0.5 * self.__coefficients[i, j, k, j] * scaling, state1, state2, state1Idx)
                        state2 = state1.applyTerm2(j, k)
                        callback.addEntry(-0.5 * self.__coefficients[i, j, k, i] * scaling, state1, state2, state1Idx)

            for i in bandSites:
                for j in bandSites:
                    callback.addEntry2(-0.25 * self.__coefficients[i, j, i, j] * scaling, state1, state1, state1Idx,
                                       state1Idx)
                    callback.addEntry2(0.25 * self.__coefficients[i, j, j, i] * scaling, state1, state1, state1Idx,
                                       state1Idx)

    def update(self):
        self.__initCoefficients()

    def __str__(self):
        return "SYK q=4 (J=" + str(self.__J) + ")"

    def __initCoefficients(self):
        """
        Fills the coefficients array with random normal distributed values and makes it hermitean.
        """
        norm = 1.0 / np.sqrt(2.0)
        self.__coefficients = np.zeros(shape=(self.__N, self.__N, self.__N, self.__N))

        for i in range(self.__N):
            for j in range(i):
                for k in range(self.__N):
                    for l in range(k):
                        if i == k and j == l:
                            self.__coefficients[i, j, k, l] = np.random.normal(loc=0.0, scale=self.__J)
                        else:
                            self.__coefficients[i, j, k, l] = norm * (np.random.normal(loc=0.0, scale=self.__J)
                                                                      + 1.0j * np.random.normal(loc=0.0,
                                                                                                scale=self.__J))

                        # hermitian and anti-commutation relation
                        self.__coefficients[j][i][k][l] = -self.__coefficients[i][j][k][l]
                        self.__coefficients[i][j][l][k] = -self.__coefficients[i][j][k][l]
                        self.__coefficients[j][i][l][k] = self.__coefficients[i][j][k][l]

                        self.__coefficients[k][l][i][j] = np.conjugate(self.__coefficients[i][j][k][l])
                        self.__coefficients[l][k][i][j] = -self.__coefficients[k][l][i][j]
                        self.__coefficients[k][l][j][i] = -self.__coefficients[k][l][i][j]
                        self.__coefficients[l][k][j][i] = self.__coefficients[k][l][i][j]

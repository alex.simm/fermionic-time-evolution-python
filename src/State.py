from typing import List


class State:
    """
    Definition: |1, ..., N> = c*_1 ...c*_N |0>
    """
    __N: int
    __coefficient: int = 1
    __bits: int

    def __init__(self, N: int, stateNumber: int):
        self.__N = N
        self.__bits = stateNumber

    def copy(self):
        state = State(self.__N, self.__bits)
        state.__coefficient = self.__coefficient
        return state

    def __eq__(self, other):
        return self.__N == other.__N and self.__bits == other.__bits and self.__coefficient == other.__coefficient

    def isZero(self) -> bool:
        """ Whether this is a zero state (not the vacuum!), i.e. the coefficient is 0. """
        return self.__coefficient == 0

    def coefficient(self) -> int:
        """ Returns this state's coefficient +1, -1, or 0. """
        return self.__coefficient

    def stateNumber(self) -> int:
        """ Returns the number of this state in the block combinatorial number system. """
        return self.__bits

    def numParticles(self) -> int:
        """ Returns the number of particle, i.e. the number of creation operators, in this state. """
        s = 0
        for i in range(self.__N):
            if self.isSiteOccupied(i):
                s += 1
        return s

    def numOccupiedSites(self, pos1: int, pos2: int) -> int:
        """ Returns the number of occupied positions in the state between the two positions (both inclusive). """
        start = min(pos1, pos2)
        end = max(pos1, pos2)

        s = 0
        for i in range(start, end + 1):
            if self.isSiteOccupied(i):
                s += 1
        return s

    def numOccupiedSitesList(self, sites: List[int]) -> int:
        """ Returns how many of the given sites are occupied. """
        s = 0
        for i in sites:
            if self.isSiteOccupied(i):
                s += 1
        return s

    def occupiedSites(self) -> List[int]:
        """ Returns a list with all site indices that are occupied by the state with the given number and charge. """
        occupied = []
        for i in range(self.__N):
            if self.isSiteOccupied(i):
                occupied.append(i)
        return occupied

    def isSiteOccupied(self, site: int) -> bool:
        """ Returns whether the site with the given index is occupied by this state. """
        return (self.__bits & (1 << site)) != 0

    def numDifferentSites(self, other) -> int:
        """ Returns the number of sites that are occupied differently in both states. """
        different = 0
        for i in range(self.__N):
            if self.isSiteOccupied(i) != other.isSiteOccupied(i):
                different += 1
        return different

    def annihilate(self, site: int):
        """ Annihilate the particle at site i. If the site is empty the state will be zero afterwards. """
        if self.__coefficient != 0:
            if self.isSiteOccupied(site):
                for i in range(site):
                    if self.isSiteOccupied(i):
                        self.__coefficient = -self.__coefficient
                self.__bits ^= (1 << site)
            else:
                self.__coefficient = 0

    def create(self, site: int):
        """ Creates a particle at site i. If the site is occupied the state will be zero afterwards. """
        if self.__coefficient != 0:
            if not self.isSiteOccupied(site):
                for i in range(site):
                    if self.isSiteOccupied(i):
                        self.__coefficient = -self.__coefficient
                toAdd = 1 << site
                self.__bits ^= toAdd
            else:
                self.__coefficient = 0

    def applyTerm2(self, i: int, j: int):
        """ Applies a hopping term: annihilates site j, creates site i, and returns the resulting state. """
        state = self.copy()
        state.annihilate(j)
        state.create(i)
        return state

    def applyTerm4(self, i: int, j: int, k: int, l: int):
        """
        Applies an interaction term: annihilates site l and k, creates site j and i, and returns the resulting state.
        """
        state = self.copy()
        state.annihilate(l)
        state.annihilate(k)
        state.create(j)
        state.create(i)
        return state

    def __str__(self):
        if self.__coefficient == 0:
            return "0"
        else:
            sign = '-' if self.__coefficient == -1 else ''
            sites = map(lambda x: str(x), self.occupiedSites())
            return f"{sign}({','.join(sites)})"

    def toBinaryString(self) -> str:
        bitSymbols = [('1' if self.isSiteOccupied(i) else '0') for i in range(self.__N-1, -1, -1)]
        return ''.join(bitSymbols)

    @staticmethod
    def combineStates(stateA, sitesA: List[int], stateB, sitesB: List[int]):
        dst = State(len(sitesA) + len(sitesB), 0)

        for i in range(stateA.__N):
            if stateA.isSiteOccupied(i):
                dst.create(sitesA[i])

        for i in range(stateB.__N):
            if stateB.isSiteOccupied(i):
                dst.create(sitesB[i])

        dst.__coefficient = 1
        return dst

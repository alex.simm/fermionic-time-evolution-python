import numpy as np


class Observable:
    """
    Base class for everything that can be measured during time evolution. Implementing classes should store measured
    values in the _values array, which can have arbitrary shape with time as the first dimension.
    """
    _values: np.ndarray = None

    def measure(self, psi: np.ndarray, step: int, numSteps: int):
        raise NotImplementedError()

    def _add(self, value: np.ndarray | float):
        if isinstance(value, float) or len(value) == 1:
            if self._values is None:
                self._values = np.empty(0)
            self._values = np.append(self._values, value)
        else:
            if self._values is None:
                self._values = np.empty((0, len(value)))
            copy = np.zeros((self._values.shape[0] + 1, self._values.shape[1]))
            copy[:-1, :] = self._values
            copy[-1, :] = value
            self._values = copy

    def getValues(self):
        return self._values

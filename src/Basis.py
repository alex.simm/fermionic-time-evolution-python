from typing import List, Dict, Set

from src.State import State


class Basis:
    __N: int  # Number of sites
    __Q: int  # Number of fermions, or -1 for a Fock space
    __states: List[State]  # Fock states
    __stateIndices: Dict[int, int]  # Maps state numbers to indices in the states vector.

    def __init__(self, N: int, Q: int = -1):
        """
        Creates a basis for N sites and Q fermions. If Q is negative the basis will contain all states with a variable
        number of fermions.
        """
        self.__N = N
        self.__Q = Q
        self.__states = []

        if Q < 0:
            size = 1 << N
            for q in range(N+1):
                for a in range(size):
                    state = State(N, a)
                    if state.numParticles() == q:
                        self.__states.append(state)
        elif Q == 1:
            for a in range(N):
                state = State(N, 1 << a)
                self.__states.append(state)
        else:
            size = 1 << N
            for a in range(size):
                state = State(N, a)
                if state.numParticles() == Q:
                    self.__states.append(state)

        # fill state index map
        self.__stateIndices = {self.__states[i].stateNumber(): i for i in range(len(self.__states))}

    def getN(self) -> int:
        """ Returns the number of sites. """
        return self.__N

    def getQ(self) -> int:
        """ Returns the number of fermions. """
        return self.__Q

    def dim(self) -> int:
        """ Returns the dimension of the Hilbert space. """
        return len(self.__states)

    def getSites(self) -> Set[int]:
        return set(list(range(self.__N)))

    def getStates(self) -> List[State]:
        """ Returns a list of all states in the Hilbert space of this Hamiltonian. """
        return self.__states

    def getState(self, index: int) -> State:
        """
        Returns the state with the given index in this model or throws an exception if no state with that index exists.
        """
        if index < 0 or index >= len(self.__states):
            raise RuntimeError(f"Invalid state index {str(index)} in basis with {str(len(self.__states))} states")
        return self.__states[index]

    def getStateIndex(self, state: State) -> int:
        """ Returns the index of the state in this model or -1 if it does not exist. """
        if state.isZero():
            return -1

        number = state.stateNumber()
        if number in self.__stateIndices:
            return self.__stateIndices[number]
        else:
            return -1

    def getStatesWithOccupiedSite(self, site: int, occupied: bool) -> List[State]:
        return list(filter(lambda state: state.isSiteOccupied(site) == occupied, self.__states))

from typing import Tuple

import numpy as np
import scipy.linalg as splin

from src.Hamiltonian import Hamiltonian
from src.evolution.TimeEvolution import TimeEvolution


class LanczosEvolution(TimeEvolution):
    __dimLanczos: int

    def __init__(self, system: Hamiltonian, dimLanczos: int):
        super().__init__(system)
        self.__dimLanczos = dimLanczos

    def evolve(self, t: float) -> None:
        Q, h = self.__createLanczosBasis()

        h *= -t * 1.0j
        tmp = Q @ splin.expm(h)
        self._psi = tmp[:, 0]

        self._update(t)

    def __createLanczosBasis(self) -> Tuple[np.ndarray, np.ndarray]:
        # basis in Krylov subspace
        Q = np.zeros(shape=(len(self._psi), self.__dimLanczos))
        # H projected into the Krylov subspace
        h = np.zeros(shape=(self.__dimLanczos, self.__dimLanczos), dtype=np.complex128)

        alpha = np.zeros(self.__dimLanczos)
        beta = np.zeros(self.__dimLanczos)
        Q[:, 0] = self._psi / np.linalg.norm(self._psi)

        # TODO: use directly instead of copy
        useSparse = self._system.isUsingSparseMatrix()
        H = self._system.asSparseMatrix() if useSparse else self._system.asMatrix()

        for j in range(1, self.__dimLanczos):
            q = H @ Q[:, j - 1]
            alpha[j - 1] = np.vdot(Q[:, j - 1], q)
            q -= alpha[j - 1] * Q[:, j - 1]
            if j > 1:
                q -= beta[j - 2] * Q[:, j - 2]

            # reorthogonalize
            delta = np.vdot(Q[:, j - 1], q)
            q -= Q[:, j - 1] * delta
            alpha[j - 1] += delta

            beta[j - 1] = np.linalg.norm(q)
            if np.abs(beta[j - 1].real) <= 1e-14:
                raise RuntimeError("Zero beta value in Lanczos routine")

            Q[:, j] = q / beta[j - 1]

        # store in the projected Hamiltonian
        h += np.diag(alpha[-self.__dimLanczos:], 0)
        h += np.diag(beta[0:self.__dimLanczos - 1], 1)
        h += np.diag(beta[0:self.__dimLanczos - 1], -1)

        # last diagonal has to be set separately
        i = Q.shape[1] - 1
        h[i, i] = np.vdot(Q[:, i], H @ Q[:, i])

        return Q, h

from typing import List

import numpy as np
import scipy.sparse as sparse

from src.Basis import Basis
from src.Hamiltonian import Hamiltonian
from src.HamiltonianBlock import HamiltonianBlock
from src.Log import Log
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm


class BlockDiagonalHamiltonian(Hamiltonian):
    """ Basis of the Hilbert space. """
    __basis: Basis
    """ Subblock ssorted in ascending particle number. """
    __blocks: List[HamiltonianBlock]
    __eigenvalues: np.ndarray | None
    __eigenvectors: np.ndarray | None

    def __init__(self, N: int, terms: List[HamiltonianTerm], sparse: bool = False):
        super().__init__(N, terms)
        self.__basis = Basis(N, -1)
        self.__eigenvalues = None
        self.__eigenvectors = None

        self.__blocks = []
        for Q in range(N + 1):
            sparseBlock = sparse and 2 < Q < len(self.__blocks) - 2
            self.__blocks.append(HamiltonianBlock(N, Q, terms, sparseBlock))

    def getBasis(self) -> Basis:
        return self.__basis

    def getBlocks(self) -> List[HamiltonianBlock]:
        return self.__blocks

    def getBlock(self, Q: int) -> HamiltonianBlock | None:
        for block in self.__blocks:
            if block.getBasis().getQ() == Q:
                return block
        return None

    def isUsingSparseMatrix(self) -> bool:
        return False

    def getSparseness(self) -> float:
        count = 0
        for block in self.__blocks:
            d = block.getBasis().dim()
            count += block.getSparseness() * (d * d)
        return count / (self.__basis.dim() ** 2)

    def isDiagonalised(self) -> bool:
        return self.__eigenvalues is not None

    def getEigenvalues(self) -> np.array:
        if not self.isDiagonalised():
            raise RuntimeError("Hamiltonian is not diagonalised")
        return self.__eigenvalues

    def getEigenvectors(self) -> np.array:
        if not self.isDiagonalised():
            raise RuntimeError("Hamiltonian is not diagonalised")
        return self.__eigenvectors

    def diagonalise(self, includeEigenvectors: bool = True, onlyGroundState: bool = False):
        dim = self.__basis.dim()
        self.__eigenvalues = np.zeros(dim)
        if includeEigenvectors:
            dim2 = 1 if onlyGroundState else self.__basis.dim()
            self.__eigenvectors = np.zeros(shape=(dim, dim2))

        off = 0
        lowestEnergy = np.inf
        for block in self.__blocks:
            Log.debug(f"diagonalising block {block.getBasis().getQ()}")
            block.diagonalise(includeEigenvectors, onlyGroundState)
            dim = block.getBasis().dim()

            if onlyGroundState:
                if block.getEigenvalues()[0] < lowestEnergy:
                    # TODO: this is maximally inefficient! improve it!
                    self.__eigenvalues[0] = block.getEigenvalues()[0]
                    self.__eigenvectors[off:off + dim, 0] = block.getEigenvectors()[:, 0]
                    lowestEnergy = self.__eigenvalues[0]
            else:
                # copy eigenvalues
                self.__eigenvalues[off:off + dim] = block.getEigenvalues()

                # copy eigenvectors
                if includeEigenvectors:
                    self.__eigenvectors[off:off + dim, off:off + dim] = block.getEigenvectors()
            off += dim

    def updateMatrix(self, updateTerms: bool):
        # terms need to be updated for all blocks at once
        if updateTerms:
            for term in self.getTerms():
                term.update()

        # update every block without updating the terms in each block
        for block in self.__blocks:
            block.updateMatrix(False)

    def asMatrix(self) -> np.ndarray:
        H = np.zeros(shape=(self.__basis.dim(), self.__basis.dim()))

        off = 0
        for block in self.__blocks:
            dim = block.getBasis().dim()
            H[off:off + dim, off:off + dim] = block.asMatrix()
            off += dim

        return H

    def asSparseMatrix(self) -> np.array:
        return sparse.csr_matrix(self.asMatrix())

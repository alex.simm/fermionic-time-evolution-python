# Fermionic Time Evolution Python

## Directory structure
- `src`: main source files
    - `hamiltonians`: classes that represent terms in a Hamiltonian, like hopping, chemical potential, or random SYK interaction
    - `simulations`: implementations of exact and Lanczos time evolution
    - `observables`: classes that represent observables and that can be added to a simulation

- `test`: unit tests

## Usage of the classes
Typical procedure to measure something during time evolution:
- Choose several terms for the Hamiltonian from `src/hamiltonians`
- Create a `Hamiltonian` instance with these terms, either a `HamiltonianBlock` for a fixed fermion number or a `BlockDiagonalHamiltonian` that includes all blocks
- Create an `TimeEvolution` with the Hamiltonian: either `ExactEvolution` or `LanczosEvolution`
- Initialise the simulation to any initial state via `TimeEvolution::setPsi`
- Choose some observables that should be measured during time evolution from `src/observables`
- Run time evolution manually by
    - repeatedly calling `TimeEvolution::evolve`
    - measuring the observables via `Observable::measure` using the state from `TimeEvolution::getPsi`
- ...or let the time evolution run automatically by using `TimeEvolution::evolveFull`, which takes a list of observables as parameters. This measures the observables in each time step.

### Example
```
N = 12 # number of band sites
Q = 6 # number of fermions, half filled
J = 1 # interaction strength of the SYK term
mu = 0.5 # chemical potential
nt = 1000 # number of time steps
dt = 0.01 # time step size
times = np.linspace(0, nt * dt, nt)

# create terms for an SYK Hamiltonian with chemical potential
sykTerm = SYK4(N, J)
chemTerm = ChemicalPotential(mu)

# create the Hamiltonian and diagonalise it
H = HamiltonianBlock(N, Q, [sykTerm, chemTerm])
H.diagonalise()

# create observables
occupation = Occupation(H.getBasis())
entropy = EntanglementEntropy(H.getBasis(), set(np.arange(N / 2)))

# create a simulation and run
sim = ExactEvolution(H)
callback = lambda step, numSteps: print(f"step {step} of {numSteps}")
sim.evolveFull(times, callback, [occupation, entropy])

# do something will the measurements
occupationValues = occupation.getValues()
entropyValues = entropy.getValues()
```
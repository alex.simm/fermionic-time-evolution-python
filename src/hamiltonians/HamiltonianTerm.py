from typing import List

from src.Basis import Basis
from src.hamiltonians.TermCallback import TermCallback


class HamiltonianTerm:
    __real: bool

    def __init__(self, real: bool):
        self.__real = real

    def isReal(self) -> bool:
        """ Whether this term will only contribute real entries to the Hamiltonian. """
        return self.__real

    def update(self):
        """
        Updates this term. Terms that depend on random numbers should override this method and reinitialise those
        numbers.
        """
        raise NotImplementedError()

    def addEntries(self, callback: TermCallback, basis: Basis, bandSites: List[int]):
        """ Adds all entries of this term to the Hamiltonian by calling the callback. """
        raise NotImplementedError()

import math
from typing import Set

import numpy as np
import scipy.linalg as splin

from src.Basis import Basis
from src.State import State
from src.observables.Observable import Observable


# TODO: test
class EntanglementEntropy(Observable):
    __ENTROPY_EIGENVALUE_CUTOFF = 1e-10
    __sitesA: Set[int]
    __sitesB: Set[int]
    __fullBasis: Basis
    __basisA: Basis
    __basisB: Basis
    __stateMatrix: np.ndarray
    __diagonalise: bool

    def __init__(self, fullBasis: Basis, sitesA: Set[int], diagonalise: bool = True):
        self.__fullBasis = fullBasis
        self.__sitesA = sitesA
        self.__sitesB = fullBasis.getSites() - sitesA
        self.__basisA = Basis(len(sitesA), -1)
        self.__basisB = Basis(len(self.__sitesB), -1)
        self.__diagonalise = diagonalise

        self.__stateMatrix = np.zeros(shape=(self.__basisA.dim(), self.__basisB.dim()), dtype=np.integer)
        for stateA in self.__basisA.getStates():
            for stateB in self.__basisB.getStates():
                combined = State.combineStates(stateA, list(self.__sitesA), stateB, list(self.__sitesB))
                index = fullBasis.getStateIndex(combined)
                if index >= 0:
                    self.__stateMatrix[self.__basisA.getStateIndex(stateA), self.__basisB.getStateIndex(stateB)] = index

    def getSitesA(self) -> Set[int]:
        return self.__sitesA

    def getSitesB(self) -> Set[int]:
        return self.__sitesB

    def __densityMatrix(self, psi: np.ndarray) -> np.ndarray:
        """ Returns the density matrix of a pure state. """
        dim = len(psi)
        return np.kron(psi, psi.T).reshape((dim, dim))

    def __partialTrace(self, psi: np.ndarray) -> np.ndarray:
        """ Returns the reduced density matrix of a pure state with subspace B traced out. """
        if len(psi) != self.__fullBasis.dim():
            raise RuntimeError(
                f"incompatible dimensions in entropy calculation: {len(psi)} != {self.__fullBasis.dim()}")

        dimA = self.__basisA.dim()
        dimB = self.__basisB.dim()

        reducedRho = np.zeros(shape=(dimA, dimA), dtype=np.complex128)
        for stateI in self.__basisA.getStates():
            i = self.__basisA.getStateIndex(stateI)
            for stateJ in self.__basisA.getStates():
                j = self.__basisA.getStateIndex(stateJ)
                for k in range(dimB):
                    idx_ik = self.__stateMatrix[i, k]
                    idx_jk = self.__stateMatrix[j, k]
                    if idx_ik >= 0 and idx_jk >= 0:
                        reducedRho[i, j] += psi[idx_ik] * np.conjugate(psi[idx_jk])

        return reducedRho

    def __entropy(self, rho: np.ndarray, diagonalise: bool = True) -> float:
        """
        Calculates the entropy S = -tr(rho * log(rho)) for the given reduced density matrix rho.

        :param rho: rho any (reduced) density matrix
        :param diagonalise: whether the entropy should be calculated from the eigenvalues of the density matrix or using
                            a matrix logarithm. Diagonalisation seems to be faster most of the time.
        """
        if rho.shape == (1, 1):
            return -(rho[0, 0] * np.log(rho[0, 0])).real

        if diagonalise:
            eigenvalues = np.linalg.eigvalsh(rho)

            # filter out NaN and very small values
            eigenvalues = eigenvalues[~np.isnan(eigenvalues)]
            eigenvalues = eigenvalues[np.abs(eigenvalues) >= self.__ENTROPY_EIGENVALUE_CUTOFF]

            return -np.sum(eigenvalues * np.log(eigenvalues))
        else:
            return -np.trace(rho * splin.logm(rho)).real

    def measure(self, psi: np.ndarray, step: int, numSteps: int):
        rhoA = self.__partialTrace(psi)
        entropy = self.__entropy(rhoA, self.__diagonalise)
        self._add(entropy)

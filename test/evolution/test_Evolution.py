import numpy as np
import numpy.testing as testing
import pytest

from src.HamiltonianBlock import HamiltonianBlock
from test.evolution.IdentityTimeEvolution import IdentityTimeEvolution
from test.hamiltonian.DiagonalHamiltonianTerm import DiagonalHamiltonianTerm


@pytest.fixture
def randomDiagonalHamiltonianBlock():
    def _method(N: int, Q: int):
        energies = np.random.rand(N)
        diagonal = DiagonalHamiltonianTerm(energies)
        return HamiltonianBlock(N, Q, [diagonal])

    return _method


def testInitialisation(randomDiagonalHamiltonianBlock):
    H = randomDiagonalHamiltonianBlock(10, 1)

    sim = IdentityTimeEvolution(H)
    assert len(sim.getPsi()) == H.getBasis().dim()
    testing.assert_array_almost_equal(sim.getPsi(), np.zeros(H.getBasis().dim()))
    assert not H.isDiagonalised()


def testSetBasisState(randomDiagonalHamiltonianBlock):
    stateIdx = 3

    H = randomDiagonalHamiltonianBlock(10, 1)
    sim = IdentityTimeEvolution(H)
    sim.setPsiState(H.getBasis().getState(stateIdx))

    psi = sim.getPsi()
    for i in range(len(psi)):
        if i == stateIdx:
            testing.assert_approx_equal(psi[i], 1)
        else:
            testing.assert_approx_equal(psi[i], 0)


def testSetNonBasisState(randomDiagonalHamiltonianBlock):
    stateIndices = [3, 4, 7]
    weights = [0.3, 0.6, 1.1]
    normalisedWeights = [0.232845, 0.46569, 0.853765]

    H = randomDiagonalHamiltonianBlock(10, 1)
    sim = IdentityTimeEvolution(H)

    states = [H.getBasis().getState(index) for index in stateIndices]
    sim.setPsiStates2(states, np.array(weights))

    psi = sim.getPsi()
    for i in range(len(psi)):
        try:
            idx = stateIndices.index(i)
            testing.assert_approx_equal(psi[i], normalisedWeights[idx], significant=5)
        except ValueError:
            testing.assert_approx_equal(psi[i], 0)

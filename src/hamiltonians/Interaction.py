from typing import List

from src.Basis import Basis
from src.hamiltonians.HamiltonianTerm import HamiltonianTerm
from src.hamiltonians.TermCallback import TermCallback


class Interaction(HamiltonianTerm):
    __strength: float

    def __init__(self, strength: float):
        super().__init__(True)
        self.__strength = strength

    def __str__(self):
        return f"interaction strength={self.__strength}"

    def getStrength(self) -> float:
        return self.__strength

    def setStrength(self, strength: float) -> None:
        self.__strength = strength

    def addEntries(self, callback: TermCallback, basis: Basis, bandSites: List[int]):
        states = basis.getStates()
        for state in states:
            stateIdx = basis.getStateIndex(state)
            for i in bandSites:
                for j in bandSites:
                    if i != j and state.isSiteOccupied(i) and state.isSiteOccupied(j):
                        callback.addEntry2(self.__strength, state, state, stateIdx, stateIdx)

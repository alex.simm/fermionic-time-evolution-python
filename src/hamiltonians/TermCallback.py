from src.State import State


class TermCallback:
    def addEntry(self, entry: float | complex, state1: State, state2: State, state1Index: int) -> None:
        raise NotImplementedError()

    def addEntry2(self, entry: float | complex, state1: State, state2: State, state1Index: int, state2Index: int) -> None:
        raise NotImplementedError()

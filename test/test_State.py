import numpy as np
import numpy.testing as testing

from src.State import State


def testConstruction():
    state1 = State(6, 0)
    assert len(state1.occupiedSites()) == 0
    assert state1.stateNumber() == 0

    state2 = State(6, 7)
    assert state2.occupiedSites() == [0, 1, 2]

    state3 = State(6, 25)
    assert state3.occupiedSites() == [0, 3, 4]


def testStateNumbers():
    for j in range(1 << 8):
        state = State(8, j)
        assert state.stateNumber() == j


def testOccupiedSites():
    for j in range(20):
        assert not State(5, 0).isSiteOccupied(j)

    assert not State(5, 2).isSiteOccupied(0)
    assert State(5, 2).isSiteOccupied(1)
    assert not State(5, 2).isSiteOccupied(2)

    for j in range(10):
        assert State(10, (1 << 10) - 1).isSiteOccupied(j)


def testNumberOfParticles():
    N = 5
    evals = np.linspace(1.0, 10.0, 10)
    assert State(N, 0).numParticles() == 0
    assert State(N, 1).numParticles() == 1
    assert State(N, 15).numParticles() == 4
    assert State(N, 10).numParticles() == 2


def testNumberOfDifferentSites():
    N = 5
    assert State(N, 1).numDifferentSites(State(N, 0)) == 1
    assert State(N, 15).numDifferentSites(State(N, 0)) == 4
    assert State(N, 16).numDifferentSites(State(N, 0)) == 1
    assert State(N, 31).numDifferentSites(State(N, 0)) == 5
    assert State(N, 31).numDifferentSites(State(N, 15)) == 1
    assert State(N, 4).numDifferentSites(State(N, 2)) == 2
    assert State(N, 14).numDifferentSites(State(N, 3)) == 3


def testNuberOfOccupiedSites():
    N = 6
    assert State(N, 1).numOccupiedSites(0, 5) == 1
    assert State(N, 1).numOccupiedSites(1, 5) == 0
    assert State(N, 15).numOccupiedSites(1, 3) == 3
    assert State(N, 16).numOccupiedSites(2, 7) == 1
    assert State(N, 31).numOccupiedSites(2, 7) == 3
    assert State(N, 31).numOccupiedSites(1, 6) == 4
    assert State(N, 4).numOccupiedSites(1, 4) == 1
    assert State(N, 14).numOccupiedSites(0, 3) == 3


def testCreationOperator():
    N = 6

    state1 = State(N, 0)
    state1.create(5)
    assert state1.coefficient() == 1
    assert state1.numParticles() == 1
    assert state1.occupiedSites() == [5]

    state2 = State(N, 2)
    state2.create(5)
    assert state2.coefficient() == -1
    assert state2.numParticles() == 2
    assert state2.occupiedSites() == [1, 5]

    state3 = State(N, 4)
    state3.create(2)
    assert state3.coefficient() == 0


def testAnnihilationOperator():
    N = 6

    state1 = State(N, 25)
    state1.annihilate(0)
    assert state1.coefficient() == 1
    assert state1.numParticles() == 2
    assert state1.occupiedSites() == [3, 4]

    state2 = State(N, 25)
    state2.annihilate(3)
    assert state2.coefficient() == -1
    assert state2.numParticles() == 2
    assert state2.occupiedSites() == [0, 4]

    state3 = State(N, 4)
    state3.annihilate(3)
    assert state3.coefficient() == 0


def testQuadraticTerm():
    state = State(6, 15)

    state2 = state.applyTerm2(4, 3)
    assert state2.stateNumber() == 23

    state3 = state.applyTerm2(4, 0)
    assert state3.stateNumber() == 30

    state4 = state.applyTerm2(3, 4)
    assert state4.isZero()


def testQuarticTerm():
    state = State(5, 15)

    state2 = state.applyTerm4(4, 3, 3, 2)
    assert state2.stateNumber() == 27

    state3 = state.applyTerm4(6, 7, 1, 2)
    assert state3.stateNumber() == 201

    state4 = state.applyTerm4(2, 2, 1, 1)
    assert state4.isZero()

    state5 = state.applyTerm4(7, 6, 5, 4)
    assert state5.isZero()


def testCombiningStates():
    stateA = State(4, 0b1010)
    stateB = State(2, 0b11)
    sitesA = [0, 1, 2, 3]
    sitesB = [4, 5]
    combined = State.combineStates(stateA, sitesA, stateB, sitesB)
    assert combined.stateNumber() == 0b111010
    assert combined.numParticles() == 4
    assert combined.occupiedSites() == [1, 3, 4, 5]
    assert combined.coefficient() == 1

    stateA = State(4, 0b1111)
    stateB = State(4, 0b0000)
    sitesA = [0, 2, 4, 6]
    sitesB = [1, 3, 5, 7]
    combined = State.combineStates(stateA, sitesA, stateB, sitesB)
    assert combined.stateNumber() == 0b01010101
    assert combined.numParticles() == 4
    assert combined.occupiedSites() == sitesA
    assert combined.coefficient() == 1
